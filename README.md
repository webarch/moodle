# Webarchitects Moodle Ansible Role

An Ansible role to [install Moodle](https://docs.moodle.org/39/en/Installation).

See also the [moosh role](https://git.coop/webarch/moosh).

## Notes

For migrations see [Moodle migration](https://docs.moodle.org/405/en/Moodle_migration#Recommended_method).

## Copyright

Copyright 2020-2025 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
